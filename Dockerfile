FROM ubuntu:18.04
COPY . /app
WORKDIR /app
RUN apt-get update \
    && apt-get install tesseract-ocr -y \
    python3 \
    #python-setuptools \
    python3-pip \
    && apt-get clean \
    && apt-get autoremove 
RUN pip3 install -r requirements.txt

RUN tesseract --version


EXPOSE 8001
CMD ["python3", "./server.py"]
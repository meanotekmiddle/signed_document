1. Установить программу распознавания текста на картинке: tesseract 4.0.0
    - https://medium.com/quantrium-tech/installing-tesseract-4-on-ubuntu-18-04-b6fcd0cbd78f
2. Установить библиотеку для работы с tesseract: 
    - pip install pytesseract
3. Обновить neuthink
4. Установить scratch
5. Создать по примеру файлы config.py, config.json
6. Обновить PIL:
    - python3 -m pip install --upgrade pip
    - python3 -m pip install --upgrade Pillow
6. Для запуска сервера запустить файл server.py

Настройка контейнера docker
1. ввести команду: 
    sudo docker build -t login/signed_doc . 
2. запуск контейнера с сервером на порту 8001:
    sudo docker run -p 8001:8001 -t login/signed_doc

Открыть в браузере страницу http://127.0.0.1:8001
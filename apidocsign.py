import tornado.web
import json
import os
import traceback
import logging
import signed_doc as sign

class ApiDocSignHandler(tornado.web.RequestHandler):
    '''
        API функция, принимающая документ pdf и возвращающая его статус: подписан/не подписан
    
        Получает файл, сохраняет его в папке uploads
        Передает ссылку на файл sign.check_document
        Результат в формате JSON передает в качестве ответа
    '''
    def post(self):
        result = {}
        try:
            file = self.request.files['file'][0]
            filename = file['filename'].split('/')[-1]
            file_path = f'{os.path.dirname(__file__)}/uploads/{filename}'

            with open(file_path, 'wb') as f:
                f.write(file['body'])

            result = sign.check_document(file_path)
        except:
            print("ERROR")
            result = {"status": "error"}   #если что-то пошло не так
            logging.log(logging.ERROR, traceback.format_exc())
        finally:
            self.write(json.dumps(result, ensure_ascii=False).encode('utf8'))
    

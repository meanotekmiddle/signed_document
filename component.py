#Модуль, в котором определяются компоненты свяязности на картинке

from neuthink.graph import basics as bs
from neuthink.graph.basics import Graph
from neuthink.graph.basics import Node
import neuthink.metaimage as Image 
from neuthink import nImage as im  
from typing import List
import config as c


def to_monochrome_img(imt: im.mvImage) -> im.mvImage:
    '''
        Возвращает картинку в черно-белых цветах без оттенков с заданным порогом

        Для преобразования изображения в оттенки серого, установить у imt свойство mode равный 'L': imt.mode = 'L'
        Далее преобразовать в монохромное изображение с заданным threshold
        и инвертировать: imt.Threshold(threshold=threshold,invert=True)
        Вернуть полученное изображение

        >>> imt = im.mvImage('files/test/one_word.png')
        >>> type(to_monochrome_img(imt)) == im.mvImage
        True
    '''
    imt.mode = 'L'                #Convert image to grayscale                             
    
    imt = imt.Threshold(threshold = c.THRESHOLD, invert = True)    #Apply adaptive threshold to convert image to monochrome

    imt = imt.Dilate()
    imt.mode='L'
     
    return imt


def AssignClass(image: Node):
    '''
        Вспомогательная ф-я для объединения компонентов для получения компонент - слов
        только для компонент ближайших к возможному полю для подписи

        Взять значение 'image' у узла, вызвать метод Area()
        если полученное значение меньше 1000, тогда
        поместить в узел значение 'letter' для ключа 'target_class', 
        иначе - 'drawing'
        Если значение 'near' не True, установить 'type' = 'not near image'
    '''

    if image['image'].Area() < 1000:
       image['target_class'] = 'letter'
    else:
       image['target_class'] = 'drawing'

    if 'near' in image and image['near'] is not True:
        image['type'] = ['not near image']


def is_possible_signature_field(width: int, height: int) -> bool:
    '''
        Возвращает True, если width и height в пределах заданного диапазона для полей с подписью

        Возвращает True, если:
        width больше c.MIN_WIDTH пикселей и меньше c.MAX_WIDTH
        height больше c.MIN_HEIGHT пикселей и меньше c.MAX_HEIGHT

        >>> is_possible_signature_field(100, 200)
        False
        >>> is_possible_signature_field(450, 150)
        True
    '''
    connect_components = True \
        if c.MAX_WIDTH > width > c.MIN_WIDTH \
            and c.MAX_HEIGHT > height > c.MIN_HEIGHT \
            and height < width else False

    return connect_components  


def mark_near_components(cur_component: Node, components: List[Node]):
    '''
        Помечает все ближайшие к компоненту cur_component компоненты из components

        Получить координаты (x0, y0), (x1, y1) cur_component по ключу 'location'
        Уменьшить х0 на c.DELTA_LEFT, а y0 на c.DELTA_UP
        Увеличить y1 на c.DELTA_DOWN, х1 на c.DELTA_RIGHT
        Проходится по всем элементам списка components
            Получить координаты этого элемента (X0, Y0), (X1, Y1) по ключу 'location'
            если X0 > x0 and Y0 > y0 and X1 < x1 and Y1 < y1:
            Тогда присвоить этому узлу значение True для параметра 'near', иначе - False
        
        >>> scene = Graph()
        >>> node = bs.Node(scene, {'type': 'image', 'location': ((5, 5), (10, 10))})
        >>> node1 = bs.Node(scene, {'type': 'image', 'location': ((5, 10), (10, 40))})
        >>> node2 = bs.Node(scene, {'type': 'image', 'location': ((500, 5), (600, 10))})
        >>> mark_near_components(node, [node1, node2])
        >>> scene.Match({'type': 'image', 'near': True})
        [{'type': 'image', 'location': ((5, 10), (10, 40)), 'id': '2', 'near': True}]
    '''
        
    (x0, y0), (x1, y1) = cur_component['location']

    x0 = x0 - c.DELTA_LEFT
    y0 = y0 - c.DELTA_UP
    x1 = x1 + c.DELTA_RIGHT
    y1 = y1 + c.DELTA_DOWN

    for component in components:
        (X0, Y0), (X1, Y1) = component['location']
        component['near'] = True if X0 > x0 and Y0 > y0 and X1 < x1 and Y1 < y1 \
            else component['near'] if 'near' in component else False


def get_connected_components(image: im.mvImage) -> List[Node]:
    '''
        Возвращает список вырезанных картинок на image, с возможными полями с подписями

        Создает basic граф
        Получает список компонент(components) на image - image.GetConnectedObjects(minsize = c.MINSIZE)
        Помещает компоненты в граф в качестве узлов: Image.BasicSceneGraphParser(Node(scene,{'image': image}), components)
        Для каждой компоненты component:
            Если для component ф-я is_possible_signature_field возвращает True
            то помечает компоненты рядом с этим компонентом: mark_near_components(component, components)
        Для каждой компоненты component выполняет AssignClass
        Получает узлы 'type': 'image' и 'near': True(near_sign_components)
        Объединяет компоненты: Image.JoinBoundingBoxes(near_sign_components, min_distance=10, comp_class='letter')
        Возвращает все узлы с 'type' = 'image'
        
        >>> len = len(get_connected_components(im.mvImage('files/OMWealth(2).jpg')))
        294
        RIGHT MERGE RUNNING
        >>> len
        26
    '''
    image = to_monochrome_img(image)
    components = image.GetConnectedObjects(minsize = c.MINSIZE)   #This gets all connected components with size>30 pixels in any dimension
    scene = Graph()             
    Image.BasicSceneGraphParser(Node(scene,{'image': image}), components) #Creates a scene tree with all images (see explanation_of_scene_tree.png for more info)"
    components = scene.Match({'type': 'image'})
    
    for component in components:
        width, height = component['image'].size
        if is_possible_signature_field(width, height): mark_near_components(component, components)
    
    for component in components:
        AssignClass(component)
    
    near_sign_components = scene.Match({'type': 'image', 'near': True})
    Image.JoinBoundingBoxes(near_sign_components, min_distance=10, comp_class='letter')

    return scene.Match({'type': 'image'})


if __name__ == "__main__":
    import doctest

    doctest.testmod()
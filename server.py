import json
import tornado
import tornado.ioloop
import tornado.web
import os
import traceback
#handlers import
from uploadview import UploadHandler
from showresult import ShowResultHandler
from apidocsign import ApiDocSignHandler

# Мне надоело что все обработчки лежат в одном файле с главным приложением
# Теперь надо обработчики выносить в отдельные файлы
# И импортировать их оттуда
# В примере импортирован AdminListHandler

settings = dict()

#cюда надо вписать обработчки и адреса по которым они будут находится
#в нашем примере есть два обрабочтичка для AJAX загрузчика и загрузчика
#через обычную форму (regularform)
application = tornado.web.Application([
    (r"/", UploadHandler),
    (r"/result", ShowResultHandler),
    (r"/api_doc_sign", ApiDocSignHandler)
],

    template_path=os.path.join(os.path.dirname(__file__), "templates"),
    static_path=os.path.join(os.path.dirname(__file__), "static"),
    autoreload=True,
    autoescape=None,
    debug=True,
    **settings
)

#Это просто не трогаем ))
if __name__ == "__main__":
    try:
        f = open("./config.json", "r")
        # ~ a = f.read()
        config = json.load(f)
        server_ip = config["server-ip"]
        server_port = config["server-port"]
        protocol = 'http://'
        host_url = protocol + server_ip + ":" + server_port + "/"
        print("start with:     " + host_url)
    except:
        print("Can not load file config.json")
        traceback.print_exc()
    else:
        print("loading server")
        server = tornado.httpserver.HTTPServer(application, max_buffer_size=10485760000)
        server.listen(int(server_port))
        print("starting")
        tornado.ioloop.IOLoop.instance().start()
        # tornado.ioloop.IOLoop.current().start()
import tornado.web
import json
import os
import scratch.forms as forms
from neuthink.metaimage import Load
import signed_doc as sign

class ShowResultHandler(tornado.web.RequestHandler):
    '''Это обработчик с таблицей информации об продуктах'''
    def get(self):
        """ 
        На вход получает имя файла вызывает функцию, которая получает данные формата:
        {'result':'signature', 'blocks':[{'name':'example.png', 'coords':'','answer':'signed'}]}
        """

        filename = self.get_argument('filename', None)

        datafile = sign.check_document(os.path.dirname(__file__) + '/uploads/' + filename)

        blocks = forms.NodeGrid({'name':'image', 'answer':'label'}, data = datafile['blocks'])
        blocks.title = ['Signature block', 'Answer']
        resultForm = forms.NodeForm({'answer':'label'}, data = datafile)
        resultForm.field_names = ['Result:']
        self.render("showresult.html", blocks = blocks, resultForm = resultForm)#таблицу нужно передать на страницу в виде параметра

    

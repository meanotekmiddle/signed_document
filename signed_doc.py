#Модуль для определения все ли поля для подпиcи в pdf документе заполнены
from neuthink.nImage  import mvImage
from typing import List, Dict, Union, Tuple
import signature as sign 
import component as comp
from neuthink.graph.basics import Node
import config as c
import logging
import traceback
import json
import os
logging.basicConfig(filename="sample.log", level=logging.ERROR)
        

def is_signed(components: List[Node]) -> bool:
    '''
        Возвращает True, если все поля для подписи заполнены, иначе - False

        У каждого элемента из components проверяет значение по ключу 'signed'
        Если для всех элементов это значаение равняется True, возвращает True, 
        иначе - False
        Если список components пустой, вернуть True 

        >>> is_signed([{'id': 0, 'signed': True}, {'id': 1, 'signed': True}])
        True
        >>> is_signed([{'id': 0, 'signed': False}, {'id': 1, 'signed': True}])
        False
        >>> is_signed([])
        True
    '''
    for i in components:
        if i['signed'] == False:
            return False

    return True   
               

def check_temp_folder() -> None:
    '''
        Создает папку temp в текущем каталоге, если ее нет

        Если папки static/temp нет, создает ее      
        
        >>> check_temp_folder() 
        >>> os.path.exists('static/temp/')
        True
    '''
    directory = 'static/temp/'

    if not os.path.exists(directory): os.makedirs(directory) 
    

def get_file_name(file_name: str) -> str:
    '''
        Возвращает название файла с расширением, но без пути к этому файлу

        Разделить file_name на элементы по символу '/', взять последний элемент

        >>> get_file_name('/manager/git.py')
        'git.py'
    '''
    return file_name.split('/')[-1]


def save_file(image: mvImage) -> str:
    '''
        Сохраняет файл image во временную папку temp, возвращает его полный путь с названием

        Получить количество файлов в папке temp
        Сохранить файл image в папку temp c названием: file(i).png,
        где i = количество файлов в папке temp
        Получить json объект из файла config.json
        Из него получить значения "server-ip" и "server-port"
        Вернуть: 'http://' + server-ip + ':' + server-port + '/static/temp/' + {file(i).png}

        >>> i = len(os.listdir('static/temp/'))
        >>> img = mvImage('files/OMWealth_full_list(2).jpg')
        >>> res = save_file(img)
        >>> i == len(os.listdir('static/temp/')) - 1
        True
    '''
    f = open("config.json", "r")
    config = json.load(f)

    i = len(os.listdir('static/temp/'))

    file_name = f'static/temp/file_{i}.png'

    image.content.save(file_name, 'PNG')

    return f'http://{config["server-ip"]}:{config["server-port"]}/{file_name}'


def save_signature_fields(components: List[Node]) -> List[Dict[str, str]]:
    '''
        Сохраняет картинку каждого поля с подписью из components и вормирует словарь на основе исходного словаря

        Для каждого словаря (comp) из components составить новый словарь с ключами:
            'answer' со значением 'signed', если значение из comp по ключу 'signed' == True, иначе 'not signed'
            'coords' со значением по ключу 'location'
            'name' cо значением, возвращающим функцией save_file(image), где image - значение по ключу 'image'
    '''
    result = []

    for component in components:
        new_component = {\
            'answer': 'signed' if component['signed'] else 'not signed',\
            'coords': "{}".format(component['location']),\
            'name': save_file(component['image'])}
        result.append(new_component)
    
    return result


def get_signature_fields(file_name: str) -> List[Node]:
    '''
        Возвращает список полей с подписями в виде списка узлов в документе file_name

        Получает картинку mvImage по file_name. Для полученной картинки:
            Получает список узлов - компонент - comp.get_connected_components, в качестве аргумента передав картинку
            Получает список узлов - компонент с подписями - sign.get_signature_field, в качестве аргументов:
            1. картинку, 2. список узлов - компонент
        Возвращает полученный список
    '''
    img = mvImage(file_name, in_memory = True, size=(c.WIDTH, c.HEIGHT))
    components = comp.get_connected_components(img)

    return sign.get_signature_field(img.content, components)

def check_document(file_name: str) -> Dict[str, Union[str, List[Dict[str, str]]]]:
    '''
        Находит в документе поля для подписи и проверяет их на наличие подписи,
        возвращает словарь с ключами: статус(подписан/нет), название документа, список полей для подписи

        Проверить наличие папки, где хранятся картинки - check_temp_folder()
        Получить поля для подписи(components) - get_signature_fields(file_name)
        Сформировать словарь: 
        'result':'signature', 
        'answer' cо значением 'signed', если is_signed(components), иначе 'not signed'
        'file_name' со значением get_file_name(file_name)
        'blocks' cо значением, возвращаемым функцией save_signature_fields, с аргументом - список полей для подписи

        >>> check_document('doc_examples_img/2-in-one.png')['blocks'][0]['answer']
        1919
        RIGHT MERGE RUNNING
        2
        no split
        'signed'
    '''
    check_temp_folder()
    components = get_signature_fields(file_name)

    return {'result': 'signature',\
            'file_name': get_file_name(file_name),\
            'blocks': save_signature_fields(components),\
            'answer': 'signed' if is_signed(components) else 'not signed'}

if __name__ == "__main__":
    import doctest

    doctest.testmod()


redo_collapse()

window.old_content = "";

function collapse()
{

  this.classList.toggle("active");
  var content = this.nextElementSibling;
  if (content.style.maxHeight){
    content.style.maxHeight = null;
  } else {
    content.style.maxHeight = content.scrollHeight + "px";
  }
}

function redo_collapse()
{
  var coll = document.getElementsByClassName("collapsible");
  var i;

  for (i = 0; i < coll.length; i++) {
    coll[i].removeEventListener('click', collapse);
    coll[i].addEventListener("click", collapse );
  }

}

function ajax_button(target, targeturl, buttonid, groupid) {

  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function() {
   if (this.readyState == 4 && this.status == 200) {

     document.getElementById(target).innerHTML =
     this.responseText;
     redo_collapse()
     var mybutton = document.getElementById(buttonid)
     var mystyle = mybutton.className;
     var allbuttons = document.getElementsByName(groupid);
     for (but in allbuttons)
     {
       allbuttons[but].className = mystyle;
     }
     mybutton.className = mystyle + "-active";

   }
 };
 xhttp.open("GET", targeturl, true);
 xhttp.send();
 window.old_content = document.getElementById(target).innerHTML;
 document.getElementById(target).innerHTML= '<div style="text-align: center; width:100%;height:100%;display: :flex;"><img style="display: block;margin-top:20%;margin-left: 40%" src="/static/img/spinner.svg" alt="spinner"></div>';

}

//File API
// getElementById
function $id(id) {
	return document.getElementById(id);
}

// initialize
function InitFileApi() {

	var fileselect = $id("fileselect"),
		filedrag = $id("filedrag"),
		submitbutton = $id("submitbutton");


  if (fileselect != null){
	// file select
	fileselect.addEventListener("change", FileSelectHandler, false);

	// is XHR2 available?
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {

		// file drop
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
		filedrag.style.display = "block";

		// remove submit button
		submitbutton.style.display = "none";
    hideblocks = document.getElementsByName('nofileapi');
    for (i = 0; i < hideblocks.length; i++) hideblocks[i].style.display = "none";
	}
}

}

// file drag hover
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.target.className = (e.type == "dragover" ? "hover" : "");
}

function uploadFile(file) {
  submiturl = $id("submiturl").value;
  var url = submiturl;
  var xhr = new XMLHttpRequest()
  var formData = new FormData()
  xhr.open('POST', url, true)

  xhr.addEventListener('readystatechange', function(e) {
    if (xhr.readyState == 4 && xhr.status == 200) {
      var body = xhr.response;
      var post_div = $id("fileupload");
      post_div.innerHTML = body;
      ///alert("Done")
      // Done. Inform the user
    }
    else if (xhr.readyState == 4 && xhr.status != 200) {
        alert("Error")
      // Error. Inform the user
    }
  })

  $id("progressbardiv").style.display = "block";
  xhr.upload.addEventListener("progress", progressHandler, false);

  formData.append('file', file)

  xhr.send(formData)
}

function progressHandler(event) {

  var percent = (event.loaded / (event.total+0.001)) * 100;
  $id("progressBar").value = Math.round(percent);

}



// file selection
function FileSelectHandler(e) {

	// cancel event and hover styling
	FileDragHover(e);

	// fetch FileList object
  var data = e.dataTransfer;
	var files = data.files;

	// process all File objects
//	for (var i = 0, f; f = files[i]; i++) {
	ParseFile(files[0]);
  uploadFile(files[0]);
//	}

}




function ParseFile(file) {
	filedrag = $id("filedrag")
  filedrag.innerHTML = file.name;
  submitbutton = $id("submitbutton");
  submitbutton.style.display = "block";


}

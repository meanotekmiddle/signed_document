import requests
import json
request_url = "http://87.117.180.230:8001/"

filename = 'doc_examples_img/2-in-one.png'

def api_requests(filename: str):
    '''
        Пример функции, обращающейся к API ф-ии, которая определяет, подписан ли документ

        Сформировать запрос: requests.post, где первым параметром передается строка: http://адрес:порт/api_doc_sign
        вторым files - словарь, где ключ - file, значение: файл pdf
        Результатом разпроса будет json объект в формате:
        {'result': 'signature', 'file_name': 'doc2.pdf', 'blocks': [
            {'answer': 'not signed', 'coords': '((206, 1587), (740, 1659))'}, 
            {'answer': 'signed', 'coords': '((881, 1160), (1416, 1234))'}
            ...], 
        'answer': 'not signed'}
        значение по ключу answer - подписан документ или нет
        значение по ключу blocks - список словарей, где каждый словарь содержит информацию об очередном
        поле для подписи в исходном документе(answer - заполнено или нет поле подписью, coords - координаты поля с подписью)

        >>> api_requests(filename)
        {'result': 'signature', 'file_name': '2-in-one.png', 'blocks': [{'answer': 'signed', 'coords': '((345, 2133), (874, 2214))', 'name': 'http://87.117.180.230:8001/static/temp/file(X).png'}], 'answer': 'signed'}
    '''
    curr_request_url = request_url + 'api_doc_sign'

    return requests.post(curr_request_url, 
        files={'file': open(filename,'rb')}).json()

if __name__ == "__main__":
    import doctest

    doctest.testmod()
   

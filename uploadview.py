import tornado.web
import json
import os

# Я сделал простую библиотеку scratch и завел в ней класс формы
# Идея в том, что форма будет рендерится с ноделиста и собираться сама со страницы тоже в него
# см. пример ниже
# класс вообще пока примитивный
# scratch можно взять в нашем репозитории и установить так же как neuthink
import scratch.forms as forms

class UploadHandler(tornado.web.RequestHandler):
    '''Это обработчик, в данном случае для формы загрузки файла'''
    def get(self):

        # Мы создаем новую форму, в которой будет одно поле ввода под названием
        # searchbox, оно будет типа editbox (поле для ввода текста)
        # и в нем будет пустой текст потому что ,data={"searchbox":""}
        search_form = forms.NodeForm({"searchbox":"editbox","filebox":"file"},data={"searchbox":"","filebox":"Загрузить файл"})
        fileform = forms.FileUploader('Drag the file here or click ',"to find", submiturl="/" )

        self.render("uploadpage.html", searchform = search_form, fileform = fileform)

    def post(self):
        directory = 'uploads'
        fileform = forms.FileUploader("", "", request =  self.request)
        print(fileform.filename)

        if not os.path.exists(directory): os.makedirs(directory) 

        fileform.save_file(directory, fileform.filename)
        #self.write("ok")

        self.redirect("/result?filename=" + fileform.filename)
